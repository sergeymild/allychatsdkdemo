package com.sergeymild.chat.utils;

import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;

import com.sergeymild.chat.AllyChat;

/**
 * Created by sergeyMild on 12/11/15.
 */
public final class Logger {
    private static String TAG = "ChatLogger";

    public static void logi(@NonNull Object... logs) {
        if (AllyChat.getInstance().isLoggingEnabled()) {
            for (int i = 0; i < logs.length; i++) {
                logs[i] = String.valueOf(logs[i]);
            }
            Log.i(TAG, TextUtils.join(", ", logs));
        }
    }

    public static void loge(@NonNull Throwable throwable) {
        if (AllyChat.getInstance().isLoggingEnabled()) {
            Log.e(TAG, "error", throwable);
        }
    }
}

package com.sergeymild.chat.utils;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.mime.TypedInput;

/**
 * Created by sergeyMild on 26/11/15.
 */
public class StringUtils {
    public static String convertInputStreamToString(TypedInput in){
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(in.in()));
            StringBuilder out = new StringBuilder();
            String newLine = System.getProperty("line.separator");
            String line;
            while ((line = reader.readLine()) != null) {
                out.append(line);
                out.append(newLine);
            }
            return out.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String toString(Object object) {
        if (object == null) {
            return "null";
        }
        try {
            return new JSONObject(new Gson().toJson(object)).toString(3);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return "";
    }

}

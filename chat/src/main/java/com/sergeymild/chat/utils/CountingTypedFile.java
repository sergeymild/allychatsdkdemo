package com.sergeymild.chat.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import retrofit.mime.TypedFile;

/**
 * Created by sergeyMild on 13/12/15.
 */
public class CountingTypedFile extends TypedFile {
    private static final int BUFFER_SIZE = 4096;
    private final ProgressListener listener;


    public interface ProgressListener {
        void transferred(long num, long max);
    }

    /**
     * Constructs a new typed file.
     *
     * @param mimeType
     * @param file
     * @throws NullPointerException if file or mimeType is null
     */
    public CountingTypedFile(String mimeType, File file, ProgressListener listener) {
        super(mimeType, file);
        this.listener = listener;
    }

    @Override public void writeTo(OutputStream out) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        FileInputStream in = new FileInputStream(super.file());
        long total = 0;
        try {
            int read;
            while ((read = in.read(buffer)) != -1) {
                total += read;
                if (this.listener != null) {
                    this.listener.transferred(total, super.file().length());
                }
                out.write(buffer, 0, read);
            }
        } finally {
            in.close();
        }
    }
}

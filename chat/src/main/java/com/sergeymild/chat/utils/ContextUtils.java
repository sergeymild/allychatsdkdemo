package com.sergeymild.chat.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;

/**
 * Created by sergeyMild on 12/11/15.
 */
public class ContextUtils {

    @NonNull
    public static String getAppVersion(@NonNull Context context) {
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
        return pInfo.versionName;
    }

    public static int getAppVersionCode(@NonNull Context context) {
        PackageInfo pInfo;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
        return pInfo.versionCode;
    }

    @NonNull
    public static String getBundleID(@NonNull Context context) {
        return context.getPackageName();
    }
}

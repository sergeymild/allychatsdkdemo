package com.sergeymild.chat.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import com.sergeymild.chat.callbacks.NetworkStateListener;
import com.sergeymild.chat.services.http.ChatUtils;


/**
 * Created by sergeyMild on 23/11/15.
 */
public class ConnectionManager implements NetworkStateListener {
    public static Context context;
    private boolean isConnected;
    private boolean isConsumed;
    private static volatile ConnectionManager Instance;

    @NonNull
    public static ConnectionManager getInstance() {
        ConnectionManager localInstance = Instance;
        if (localInstance == null) {
            synchronized (ConnectionManager.class) {
                localInstance = Instance;
                if (localInstance == null) {
                    Instance = localInstance = new ConnectionManager();
                }
            }
        }
        return localInstance;
    }

    private ConnectionManager() {
        isConnected = isNetworkOnline();
        isConsumed = isConnected;
    }

    public static boolean isNetworkOnline() {
        if (context == null) return false;
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && (netInfo.isConnectedOrConnecting() || netInfo.isAvailable())) {
                return true;
            }

            netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                return true;
            } else {
                netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if (netInfo != null && netInfo.isConnectedOrConnecting()) {
                    return true;
                }
            }
        } catch (Exception e) {
            Logger.loge(e);
            return true;
        }
        return false;
    }

    public boolean isOnline() {
        return isConnected;
    }

    @Override
    public void onNetworkStateChanged(boolean isConnected) {
        if (!isConnected && isConsumed) {
            isConsumed = false;
            this.isConnected = false;
            ChatUtils.notifyNetworkStateChanged(false);
            Logger.logi("application is offline");
        }

        if (isConnected && !isConsumed) {
            this.isConnected = true;
            isConsumed = true;
            ChatUtils.notifyNetworkStateChanged(true);
            Logger.logi("application is online");
        }
    }
}

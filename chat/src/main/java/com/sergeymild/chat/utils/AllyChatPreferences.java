package com.sergeymild.chat.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.User;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AllyChatPreferences {
    private static final String PACKAGE = "com.sergeymild.chat";
    private static final String ALIAS_NAME = PACKAGE + ".alias";
    private static final String MESSAGE_LIST = PACKAGE + ".message_list";
    private static final String USER = PACKAGE + ".models.user";
    private static final String GCM_TOKEN = PACKAGE + ".gcm_token";
    private static final String PREFERENCES_NAME = PACKAGE + ".allychat.prefrences";
    private static android.content.SharedPreferences preferences;
    private static Gson GSON = new Gson();
    static Type typeOfObject = new TypeToken<ArrayList<Message>>() {
    }.getType();
    private static boolean isInitialized;

    public static void init(Context context) {
        if (!isInitialized) {
            preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
            isInitialized = true;
        }
    }

    public static String getAlias() {
        return preferences.getString(ALIAS_NAME, "");
    }

    public static void setUser(User user) {
        preferences.edit().putString(USER + getAlias(), new Gson().toJson(user)).apply();
    }

    public static void setGcmToken(@NonNull String token) {
        preferences.edit().putString(GCM_TOKEN + getAlias(), token).apply();
    }

    @Nullable
    public static String getGcmToken() {
        return preferences.getString(GCM_TOKEN + getAlias(), null);
    }

    @Nullable
    public static User getUser() {
        String userString = preferences.getString(USER + getAlias(), null);
        if (TextUtils.isEmpty(userString)) {
            return null;
        } else {
            return new Gson().fromJson(userString, User.class);
        }
    }

    public static void setAlias(String alias) {
        preferences.edit().putString(ALIAS_NAME, alias).apply();
    }

    public static void saveLastMessageId(String roomId, String lastMessgeId) {
        preferences.edit().putString(roomId + getAlias(), lastMessgeId).apply();
    }

    @Nullable
    public static String getLastMessageId(@NonNull String roomId) {
        return preferences.getString(roomId + getAlias(), null);
    }

    public static void removeLastMessageId(@NonNull String roomId) {
        preferences.edit().remove(roomId + getAlias()).apply();
    }


    public synchronized static void updateMessage(@NonNull Message message) {
        Logger.logi("logger", "updateToList");
        String stringList = preferences.getString(MESSAGE_LIST + getAlias(), null);
        ArrayList<Message> messages;
        messages = GSON.fromJson(stringList, typeOfObject);

        if (messages != null) {
            for (int i = 0; i < messages.size(); i++) {
                Logger.logi("updateMessage " + message.getMessage());
                if (messages.get(i).getLocalId().equals(message.getLocalId())) {
                    Logger.logi("nees remove " + message.getMessage());
                    messages.remove(i);
                }
            }
        }
        messages.add(message);
        preferences.edit().putString(MESSAGE_LIST + getAlias(), GSON.toJson(messages)).apply();
    }

    public synchronized static void addToList(@NonNull Message message) {
        Logger.logi("logger", "addToList " + message.getLocalId());
        String stringList = preferences.getString(MESSAGE_LIST + getAlias(), null);
        ArrayList<Message> messages;
        if (stringList == null) {
            messages = new ArrayList<Message>();
        } else {
            messages = GSON.fromJson(stringList, typeOfObject);
        }

        boolean contains = false;

        for (Message inList : messages) {
            contains = inList.getLocalId().equals(message.getLocalId());
        }

        if (!contains) {
            Logger.logi("logger", "addToList message not contains adding");
            messages.add(message);
        }
        Logger.logi("logger", "addToList not sended messages count ", messages.size());
        preferences.edit().putString(MESSAGE_LIST + getAlias(), GSON.toJson(messages)).apply();
    }

    public static void removeFromList(@NonNull Message newMessage) {
        Logger.logi("removeFromList method " + newMessage.getLocalId());
        List<Message> list = getList();
        if (list == null) return;
        Iterator<Message> iterator = list.iterator();
        while (iterator.hasNext()) {
            Message next = iterator.next();
            if (next.getLocalId() != null && next.getLocalId().equals(newMessage.getLocalId())) {
                Logger.logi("logger", "messages equals remove " + newMessage.getLocalId());
                iterator.remove();
            }
        }
        Logger.logi("logger", "removeFromList not sended messages count ", list.size());
        preferences.edit().putString(MESSAGE_LIST + getAlias(), GSON.toJson(list)).apply();
    }

    public static void removeList() {
        preferences.edit().remove(MESSAGE_LIST + getAlias()).apply();
    }

    public static void clear() {
        preferences.edit().clear().apply();
    }

    @Nullable
    public static List<Message> getList() {
        return GSON.fromJson(preferences.getString(MESSAGE_LIST + getAlias(), null), typeOfObject);
    }
}

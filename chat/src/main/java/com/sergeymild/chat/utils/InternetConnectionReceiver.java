package com.sergeymild.chat.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by sergeyMild on 21/11/15.
 */
public class InternetConnectionReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        NetworkInfo currentNetworkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
        ConnectionManager.context = context.getApplicationContext();
        ConnectionManager.getInstance().onNetworkStateChanged(currentNetworkInfo.isConnected());
    }
}

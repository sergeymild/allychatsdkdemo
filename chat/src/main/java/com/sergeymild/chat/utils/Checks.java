package com.sergeymild.chat.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.sergeymild.chat.AllyChat;
import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.callbacks.OnFailureInitialize;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.Errors;
import com.sergeymild.chat.models.User;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.services.http.ServiceFactory;

/**
 * Created by sergeyMild on 03/12/15.
 */
public final class Checks {

    private Checks() {
        throw new IllegalStateException("No instances please.");
    }

    public static boolean checkOnline() {
        if (!ConnectionManager.isNetworkOnline()) {
            ChatUtils.notifyOnError(new ErrorState(Errors.CHAT_IS_NOT_CONNECTED.message, Errors.CHAT_IS_NOT_CONNECTED.status));
            return false;
        }
        return true;
    }

    public static boolean checkOnline(@Nullable OnFailureInitialize onFailureInitialize) {
        if (!ConnectionManager.isNetworkOnline()) {
            if (onFailureInitialize != null) {
                onFailureInitialize.onFailInitialize(new ErrorState(Errors.CHAT_IS_NOT_CONNECTED.message, Errors.CHAT_IS_NOT_CONNECTED.status));
            }
            return false;
        }
        return true;
    }

    public static boolean checkConnected() {
        return ServiceFactory.getWsChatApi().isConnected() || ConnectionManager.isNetworkOnline();
    }

    /**
     * Checks that passed string is not null and not empty,
     * throws {@link NullPointerException} or {@link IllegalStateException} with passed message
     * if string is null or empty.
     *
     * @param value   a string to check
     * @param message exception message if object is null
     */
    public static void checkNotEmpty(@Nullable String value, @NonNull String message) {
        if (value == null) {
            throw new NullPointerException(message);
        } else if (value.length() == 0) {
            throw new IllegalStateException(message);
        }
    }

    public static <T> boolean checkAppInitialized(@Nullable ChatCallback<T> callback) {
        if (!AllyChat.getInstance().isInitialized()) {
            ErrorState errorState = new ErrorState(Errors.FAILED_APP_IN_NOT_INITIALIZED.message, Errors.FAILED_APP_IN_NOT_INITIALIZED.status);
            if (callback == null) {
                ChatUtils.notifyOnError(errorState);
            } else {
                callback.failure(errorState);
            }
            return false;
        }
        return true;
    }

    public static void throwIfAppNotInitialized() {
        if (!AllyChat.getInstance().isInitialized()) {
            throw new IllegalStateException(Errors.FAILED_APP_IN_NOT_INITIALIZED.message);
        }
    }


    public static boolean checkUser(ChatCallback callback) {
        User user = AllyChat.getInstance().getUser();
        if (user == null || user.getId() == null) {
            callback.failure(new ErrorState(Errors.USER_IS_NOT_REGISTERED.message, Errors.USER_IS_NOT_REGISTERED.status));
            return false;
        }
        return true;
    }
}

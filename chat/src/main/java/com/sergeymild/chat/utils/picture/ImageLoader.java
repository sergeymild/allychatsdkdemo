package com.sergeymild.chat.utils.picture;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import com.sergeymild.chat.AllyChat;


/**
 * Created by sergeyMild on 22/11/15.
 */
public class ImageLoader {
    private static volatile ImageLoader Instance = null;

    public static ImageLoader getInstance() {
        ImageLoader localInstance = Instance;
        if (localInstance == null) {
            synchronized (ImageLoader.class) {
                localInstance = Instance;
                if (localInstance == null) {
                    Instance = localInstance = new ImageLoader();
                }
            }
        }
        return localInstance;
    }

    private ImageLoader() {}

    public static Bitmap loadBitmap(@Nullable String path, float maxWidth, float maxHeight, boolean useMaxScale) {
        if (TextUtils.isEmpty(path)) return null;
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(path, bmOptions);
        float photoW = bmOptions.outWidth;
        float photoH = bmOptions.outHeight;
        float scaleFactor = useMaxScale ? Math.max(photoW / maxWidth, photoH / maxHeight) : Math.min(photoW / maxWidth, photoH / maxHeight);
        if (scaleFactor < 1) {
            scaleFactor = 1;
        }
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = (int) scaleFactor;

        Matrix matrix = null;

        ExifInterface exif;
        try {
            exif = new ExifInterface(path);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
            }
        } catch (Throwable e) {
            Log.e("tmessages", "", e);
        }

        Bitmap b = null;
        try {
            b = BitmapFactory.decodeFile(path, bmOptions);
            if (b != null) {
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
            }
        } catch (Throwable e) {
            Log.e("tmessages", "", e);
            try {
                if (b == null) {
                    b = BitmapFactory.decodeFile(path, bmOptions);
                }
                if (b != null) {
                    b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
                }
            } catch (Throwable e2) {
                Log.e("tmessages", "", e2);
            }
        }

        return b;
    }

    public static String getPath(Intent data, Uri uri) {
        String path = null;
        if (data != null && data.getData() != null) {
            final Uri dataUri = data.getData();
            path = TakePhotoUtils.getPath(AllyChat.getInstance().getContext(), dataUri);

        }
        if (path == null) {
            path = TakePhotoUtils.getPath(AllyChat.getInstance().getContext(), uri);
        }
        return path;
    }
}

package com.sergeymild.chat.utils;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

/**
 * Created by sergeyMild on 12/11/15.
 */
public class DispatchUtil {
    @NonNull
    public final DispatchQueue networkQueue;
    @NonNull
    public final DispatchQueue backgroundQueue;
    @NonNull
    private Handler mHandler;

    public static class ExecutorHolder {
        private static final DispatchUtil INSTANCE = new DispatchUtil();
    }

    public static DispatchUtil getInstance() {
        return ExecutorHolder.INSTANCE;
    }


    private DispatchUtil() {
        networkQueue = new DispatchQueue("networkOperation", Thread.MAX_PRIORITY);
        backgroundQueue = new DispatchQueue("backgroundQueue", Thread.MIN_PRIORITY);
        mHandler = new Handler(Looper.getMainLooper());
    }

    @WorkerThread
    public void runOnUiThread(Runnable runnable) {
        mHandler.post(runnable);
    }

    @WorkerThread
    public void runOnUiThread(Runnable runnable, int delay) {
        mHandler.postDelayed(runnable, delay);
    }

    @CheckResult
    public boolean isMainThread() {
        return Looper.getMainLooper() == Looper.myLooper();
    }
}

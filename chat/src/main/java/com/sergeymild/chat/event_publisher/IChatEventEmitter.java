package com.sergeymild.chat.event_publisher;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import com.sergeymild.chat.callbacks.NetworkStateListener;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.Message;

/**
 * Created by sergeyMild on 14/01/16.
 */
public interface IChatEventEmitter {
    void register(@NonNull final Object object);
    void unregister(@NonNull final Object object);
    void unregisterAll();
    void subscribeOnNetworkStateChange(@NonNull final NetworkStateListener listener);
    void unSubscribeOnNetworkStateChange(@NonNull NetworkStateListener listener);
    void postNetworkStateChanged(final boolean isConnected);
    void subscribeOnError(@NonNull final OnError<ErrorState> listener);
    void unSubscribeOnError(@NonNull final OnError<ErrorState> listener);
    void postOnError(@NonNull final ErrorState errorState);
    @WorkerThread
    void onMessageStatusChanged(@NonNull final Message message);
    @WorkerThread
    void onMessageEventStatusChanged(@NonNull final Message message);
    @WorkerThread
    void onMessage(@NonNull final Message message);
}

package com.sergeymild.chat.event_publisher;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.sergeymild.chat.callbacks.NetworkStateListener;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.utils.DispatchUtil;
import com.sergeymild.chat.utils.Logger;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by sergeyMild on 20/11/15.
 */
public final class ChatEventEmitter implements IChatEventEmitter {
    private final List<OnMessage> observers = new ArrayList<OnMessage>();
    private final List<NetworkStateListener> networkStateListeners = new ArrayList<NetworkStateListener>();
    private final List<OnError<ErrorState>> onErrorListeners = new ArrayList<OnError<ErrorState>>();

    private static class InstanceHolder {
        private static final ChatEventEmitter INSTANCE = new ChatEventEmitter();
    }

    public static ChatEventEmitter getInstance() {
        return InstanceHolder.INSTANCE;
    }


    @Override
    public final void register(@NonNull final Object object) {
        Class<?>[] interfaces = object.getClass().getInterfaces();
        for (Class<?> anInterface : interfaces) {
            if (anInterface.getCanonicalName().equals(NetworkStateListener.class.getCanonicalName())) {
                Logger.logi("register NetworkState listener");
                subscribeOnNetworkStateChange((NetworkStateListener) object);
            }

            if (anInterface.getCanonicalName().equals(OnMessage.class.getCanonicalName())) {
                Logger.logi("register OnMesage listener");
                subscribeOnMessage((OnMessage) object);
            }

            if (anInterface.getCanonicalName().equals(OnError.class.getCanonicalName())) {
                Logger.logi("register OnError listener");
                subscribeOnError((OnError<ErrorState>) object);
            }
        }
    }

    @Override
    public final void unregister(@NonNull final Object object) {
        Class<?>[] interfaces = object.getClass().getInterfaces();
        for (Class<?> anInterface : interfaces) {
            if (anInterface.getCanonicalName().equals(NetworkStateListener.class.getCanonicalName())) {
                unSubscribeOnNetworkStateChange((NetworkStateListener) object);
                Logger.logi("unRegister NetworkStateListener listener");
            }

            if (anInterface.getCanonicalName().equals(OnMessage.class.getCanonicalName())) {
                unSubscribeOnMessage((OnMessage) object);
                Logger.logi("unRegister OnMessage listener");
            }

            if (anInterface.getCanonicalName().equals(OnError.class.getCanonicalName())) {
                unSubscribeOnError((OnError<ErrorState>) object);
                Logger.logi("unRegister OnError listener");
            }
        }
    }

    @Override
    public final void unregisterAll() {
        observers.clear();
        networkStateListeners.clear();
    }

    private void subscribeOnMessage(@NonNull final OnMessage onMessage) {
        if (!observers.contains(onMessage)) observers.add(onMessage);
    }

    private void unSubscribeOnMessage(@NonNull final OnMessage onMessage) {
        observers.remove(onMessage);
    }

    @Override
    public final void subscribeOnNetworkStateChange(@NonNull final NetworkStateListener listener) {
        if (!networkStateListeners.contains(listener)) networkStateListeners.add(listener);
    }

    @Override
    public final void unSubscribeOnNetworkStateChange(@NonNull NetworkStateListener listener) {
        networkStateListeners.remove(listener);
    }

    @Override
    public final void postNetworkStateChanged(final boolean isConnected) {
        for (NetworkStateListener networkStateListener : networkStateListeners) {
            networkStateListener.onNetworkStateChanged(isConnected);
        }
    }

    @Override
    public final void subscribeOnError(@NonNull final OnError<ErrorState> listener) {
        if (!onErrorListeners.contains(listener)) onErrorListeners.add(listener);
    }

    @Override
    public void unSubscribeOnError(@NonNull final OnError<ErrorState> listener) {
        onErrorListeners.remove(listener);
    }

    @Override
    public final void postOnError(@NonNull final ErrorState errorState) {
        for (OnError<ErrorState> onError : onErrorListeners) {
            onError.onError(errorState);
        }
    }


    @WorkerThread @Override
    public final void onMessageStatusChanged(@NonNull final Message message) {
        for (final OnMessage observer : observers) {
            if (DispatchUtil.getInstance().isMainThread()) {
                observer.onMessageStatusChanged(message);
            } else {
                DispatchUtil.getInstance().runOnUiThread(new Runnable() {
                    @Override public void run() {
                        observer.onMessageStatusChanged(message);
                    }
                });
            }
        }
    }

    @WorkerThread @Override
    public final void onMessageEventStatusChanged(@NonNull final Message message) {
        for (final OnMessage observer : observers) {
            if (DispatchUtil.getInstance().isMainThread()) {
                observer.onMessageEventChanged(message);
            } else {
                DispatchUtil.getInstance().runOnUiThread(new Runnable() {
                    @Override public void run() {
                        observer.onMessageEventChanged(message);
                    }
                });
            }
        }
    }

    @WorkerThread @Override
    public void onMessage(@NonNull final Message message) {
        for (final OnMessage observer : observers) {
            if (DispatchUtil.getInstance().isMainThread()) {
                observer.onMessage(message);
            } else {
                DispatchUtil.getInstance().runOnUiThread(new Runnable() {
                    @Override public void run() {
                        observer.onMessage(message);
                    }
                });
            }
        }
    }
}

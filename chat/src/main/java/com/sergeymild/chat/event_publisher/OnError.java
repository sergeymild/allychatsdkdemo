package com.sergeymild.chat.event_publisher;

/**
 * Created by sergeyMild on 20/11/15.
 */
public interface OnError<T> {
    void onError(T t);
}

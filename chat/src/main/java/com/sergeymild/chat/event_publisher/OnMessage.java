package com.sergeymild.chat.event_publisher;

import android.support.annotation.NonNull;
import com.sergeymild.chat.models.Message;

/**
 * Created by sergeyMild on 20/11/15.
 */
public interface OnMessage {
    void onMessage(@NonNull Message message);
    void onMessageStatusChanged(@NonNull Message message);
    void onMessageEventChanged(@NonNull Message message);

    abstract class SimpleOnMessage implements OnMessage {
        @Override
        public void onMessageStatusChanged(@NonNull Message message) {}

        @Override
        public void onMessageEventChanged(@NonNull Message message) {}
    }
}

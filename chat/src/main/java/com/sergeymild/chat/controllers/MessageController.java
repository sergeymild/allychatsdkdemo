package com.sergeymild.chat.controllers;

import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.sergeymild.chat.callbacks.SimpleChatCallback;
import com.sergeymild.chat.utils.CountingTypedFile;
import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.MessageFile;
import com.sergeymild.chat.models.MessageStatus;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.services.http.ServiceFactory;
import com.sergeymild.chat.utils.AllyChatPreferences;
import com.sergeymild.chat.utils.DispatchUtil;
import com.sergeymild.chat.models.Errors;
import com.sergeymild.chat.utils.Logger;
import com.sergeymild.chat.utils.picture.TakePhotoUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import retrofit.RetrofitError;
import retrofit.mime.TypedFile;

import static com.sergeymild.chat.utils.Checks.checkConnected;

/**
 * Created by sergeyMild on 01/12/15.
 */
public final class MessageController extends BaseController  {
    public static List<Message> staticSendingMessages = new ArrayList<>();
    //private static int staticOffset = 0;
    private static AtomicInteger atomicInteger = new AtomicInteger();
    public synchronized static void setSendingMessages() {
        Logger.logi("setSendingMessages from cache");
        if (atomicInteger.get() == 0) {
            List<Message> list = AllyChatPreferences.getList();
            if (list != null) staticSendingMessages.addAll(list);
        }
    }

    public synchronized static void clearCurrentSession(boolean clearOffset) {
        Logger.logi("clearCurrentSession");
        staticSendingMessages.clear();
        //staticOffset = 0;
        if (clearOffset) resetCounter();
    }

    public static void resetCounter() {
        atomicInteger.getAndSet(0);
    }

    public static void incrementCounter() {
        Logger.logi("incrementCounter", atomicInteger.get());
        atomicInteger.incrementAndGet();
    }

    public static void decrementCounter() {
        Logger.logi("decrementCounter", atomicInteger.get());
        atomicInteger.decrementAndGet();
    }


    /*
    *
    *   GET Messages
    *
    * */

    public static void getMessages(final String roomId, final boolean stackFromEnd, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        getMoreMessages(roomId, stackFromEnd, limit, callback);
    }

    public static void getMoreMessages(@NonNull final String roomId, final boolean stackFromEnd, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Message> messages = ServiceFactory.getChatApi().getMoreMessages(roomId, stackFromEnd, limit, atomicInteger.get()/*staticOffset*/);

                    if (messages.size() >= 1) {
                        mergeList(stackFromEnd, messages);
                    }

                    int lengthLimit = limit > staticSendingMessages.size() ? staticSendingMessages.size() : limit;
                    List<Message> subList = new ArrayList<Message>(lengthLimit);
                    for (int i = 0; i < lengthLimit; i++) {
                        subList.add(staticSendingMessages.get(i));
                    }

                    if (lengthLimit == staticSendingMessages.size()) {
                        staticSendingMessages.clear();
                    } else {
                        int index = 0;
                        Iterator<Message> iterator = staticSendingMessages.iterator();
                        while (iterator.hasNext()) {
                            iterator.next();
                            iterator.remove();
                            index++;
                            if (index == lengthLimit) break;
                        }
                    }
                    Logger.logi("after remove in staticSendingMessages" + staticSendingMessages.size());

                    if (stackFromEnd) Collections.reverse(subList);
                    sendResultToUser(new ArrayList<Message>(subList), callback);
                    atomicInteger.addAndGet(messages.size());
                } catch (final Exception error) {
                    if (error instanceof RetrofitError) {
                        reLoginIfNeed((RetrofitError) error, callback, new Runnable() {
                            @Override
                            public void run() {
                                getMoreMessages(roomId, stackFromEnd, limit, callback);
                            }
                        });
                    } else {
                        callback.failure(new ErrorState(error.getMessage(), -1));
                    }
                }
            }
        });
    }

    public static void readMessage(@NonNull final String messageId) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    ServiceFactory.getChatApi().readMessage(messageId);
                } catch (Exception error) {
                    if (error instanceof RetrofitError) {
                        reLoginIfNeed((RetrofitError) error, new SimpleChatCallback<>(), new Runnable() {
                            @Override
                            public void run() {
                                readMessage(messageId);
                            }
                        });
                    } else {
                        Logger.logi("Cant relogin and read message");
                    }
                }
            }
        });
    }

    public static void sendMessage(@NonNull final Message message) {
        sendMessage(message, null);
    }

    public static void sendMessage(@NonNull final Message message, final CountingTypedFile.ProgressListener progressListener) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                ChatUtils.notifyMessageStatusChanged(message);
                AllyChatPreferences.addToList(message);
                if (!checkConnected()) {
                    message.setStatus(MessageStatus.STATUS_FAILED);
                    AllyChatPreferences.updateMessage(message);
                    Logger.logi("status must be failed", message.getNameStatus());
                    Logger.logi("sendMessage", "not connecetd", message.getStatus());
                    ChatUtils.notifyMessageStatusChanged(message);
                    ChatUtils.notifyOnError(new ErrorState(Errors.CHAT_IS_NOT_CONNECTED.message, Errors.CHAT_IS_NOT_CONNECTED.status));
                    ChatUtils.onMessage(message);
                    return;
                }



                trySendMessageImage(message, progressListener, new Runnable(){
                    @Override
                    public void run() {
                        ServiceFactory.getWsChatApi().sendMessage(message);
                        incrementCounter();
                    }
                });
            }
        });
    }


    private static void mergeList(final boolean stackFromEnd, @NonNull final List<Message> messages) {
        Set<Message> tmpUniqueSet = new HashSet<Message>();
        tmpUniqueSet.addAll(messages);
        tmpUniqueSet.addAll(staticSendingMessages);
        ArrayList<Message> sendingMessages = new ArrayList<>(tmpUniqueSet);
        Collections.sort(sendingMessages, new Comparator<Message>() {
            @Override
            public int compare(@NonNull Message lhs, @NonNull Message rhs) {
                Long lhsCreatedAtMillis = lhs.getCreatedAtMillis();
                Long rhsCreatedAtMillis = rhs.getCreatedAtMillis();
                int comparedValue;

                if (stackFromEnd) {
                    comparedValue = lhsCreatedAtMillis.compareTo(rhsCreatedAtMillis);
                } else {
                    comparedValue = rhsCreatedAtMillis.compareTo(lhsCreatedAtMillis);
                }

                if (comparedValue > 0)
                    return -1;
                else if (comparedValue < 0)
                    return 1;
                else
                    return 0;
            }
        });
        staticSendingMessages.clear();
        staticSendingMessages.addAll(sendingMessages);
    }

    public static void getMoreByLastMessageIdMessages(@NonNull final String roomId, final int limit, @NonNull final String lastMessageId, @NonNull final ChatCallback<List<Message>> callback) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Message> messages = ServiceFactory.getChatApi().getMoreByLastMessageIdMessages(roomId, false, limit, lastMessageId);
                    sendResultToUser(messages, callback);
                } catch (final Exception error) {
                    if (error instanceof RetrofitError) {
                        reLoginIfNeed((RetrofitError) error, callback, new Runnable() {
                            @Override
                            public void run() {
                                getMoreByLastMessageIdMessages(roomId, limit, lastMessageId, callback);
                            }
                        });
                    } else {
                        callback.failure(new ErrorState(error.getMessage(), -1));
                    }
                }
            }
        });
    }


    private static void trySendMessageImage(@NonNull final Message message, final CountingTypedFile.ProgressListener progressListener, final Runnable successRunnable) {
        if (!TextUtils.isEmpty(message.getFile())) {
            File file = new File(TakePhotoUtils.decodeFile(message.getFile(), 750, 600));
            TypedFile typedFile = new CountingTypedFile("multipart/form-data", file, progressListener);
            try {
                MessageFile messageFile = ServiceFactory.getChatApi().uploadFile(typedFile);
                message.setFile(messageFile.getUrl());
                successRunnable.run();
            } catch (Exception error) {
                if (error instanceof RetrofitError) {
                    reLoginIfNeed((RetrofitError) error, new SimpleChatCallback<>(), new Runnable() {
                        @Override
                        public void run() {
                            trySendMessageImage(message, progressListener, successRunnable);
                        }
                    });
                } else {
                    message.setFile(null);
                    message.setStatus(MessageStatus.STATUS_FAILED);
                    ChatUtils.notifyMessageStatusChanged(message);
                    ChatUtils.notifyOnError(new ErrorState(Errors.FAILED_TO_UPLOAD_FILE.message, Errors.FAILED_TO_UPLOAD_FILE.status));
                }
            }
        } else {
            successRunnable.run();
        }
    }
}

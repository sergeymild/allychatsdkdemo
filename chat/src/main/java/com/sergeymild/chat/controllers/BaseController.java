package com.sergeymild.chat.controllers;

import android.support.annotation.NonNull;
import android.util.Log;

import com.sergeymild.chat.AllyChat;
import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.models.SdkSettings;
import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.utils.DispatchUtil;
import retrofit.RetrofitError;

/**
 * Created by sergeyMild on 01/12/15.
 */
public class BaseController {
    protected interface OnError {
        <T> void onError(RetrofitError error, ChatCallback<T> callback);
    }

    public static OnError onError = new OnError() {
        @Override
        public <T> void onError(RetrofitError error, ChatCallback<T> callback) {
            sendErrorToUser(error, callback);
        }
    };

    protected static <T> void sendResultToUser(@NonNull final T messages, @NonNull final ChatCallback<T> callback) {

        DispatchUtil.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                callback.success(messages);
            }
        });

    }

    protected static <T> void sendErrorToUser(@NonNull final RetrofitError throwable, @NonNull final ChatCallback<T> callback) {
        DispatchUtil.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                callback.failure(throwable);
            }
        });

    }

    protected static <T> void reLoginIfNeed(RetrofitError error, ChatCallback<T> callback, Runnable runAgain) {
        Log.d("logger", "called reLoginIfNeed");
        if ((error.getResponse() != null && error.getResponse().getStatus() == 401) || error.getMessage().equals("No authentication challenges found") || error.getMessage().equals("Invalid or expired token") || error.getMessage().equals("401 Unauthorized")) {
            Log.d("logger", "need reLogin");
            SdkSettings sdkSettings = SdkSettings.getInstance();
            try {
                TokenResponse token = ChatUtils.getToken(sdkSettings.getAlias(), sdkSettings.getAppId());
                Log.d("logger", "token received");
                AllyChat.getInstance().setToken(token);
                runAgain.run();
            } catch (RetrofitError error1) {
                Log.d("logger", "token received");
                onError.onError(error, callback);
            }
        } else {
            onError.onError(error, callback);
        }
    }
}

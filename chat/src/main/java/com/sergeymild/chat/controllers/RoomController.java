package com.sergeymild.chat.controllers;

import android.support.annotation.NonNull;

import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.models.Room;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.services.http.ServiceFactory;
import com.sergeymild.chat.utils.DispatchUtil;
import com.sergeymild.chat.utils.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.List;

/**
 * Created by sergeyMild on 01/12/15.
 */
public final class RoomController extends BaseController {

    public static void getSupportRoom(@NonNull final ChatCallback<Room> callback) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    Room supportRoom = ServiceFactory.getChatApi().getSupportRoom();
                    sendResultToUser(supportRoom, callback);
                } catch (final RetrofitError error) {
                    reLoginIfNeed(error, callback, new Runnable() {
                        @Override
                        public void run() {
                            getSupportRoom(callback);
                        }
                    });
                }
            }
        });
    }


    public static void getRooms(@NonNull final ChatCallback<List<Room>> callback) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Room> rooms = ServiceFactory.getChatApi().getRooms();
                    sendResultToUser(rooms, callback);
                } catch (final RetrofitError error) {
                    reLoginIfNeed(error, callback, new Runnable() {
                        @Override
                        public void run() {
                            getRooms(callback);
                        }
                    });
                }
            }
        });
    }

    public static void getUnreadCount(@NonNull final String roomId, @NonNull final ChatCallback<Integer> callback) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                try {
                    Response response = ServiceFactory.getChatApi().getUnreadCount(roomId);
                    int count = new JSONObject(StringUtils.convertInputStreamToString(response.getBody())).optInt("count", 0);
                    sendResultToUser(count, callback);
                } catch (RetrofitError error) {
                    reLoginIfNeed(error, callback, new Runnable() {
                        @Override
                        public void run() {
                            getUnreadCount(roomId, callback);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

package com.sergeymild.chat.convertors;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sergeymild.chat.models.Message;


/**
 * Created by sergeyMild on 21/11/15.
 */
public class ParseFromString {
    @CheckResult @NonNull @WorkerThread
    public Message parse(@NonNull String messageString) {

        JsonParser parser = new JsonParser();
        JsonObject jsonObject = parser.parse(messageString).getAsJsonObject();
        return new Gson().fromJson(jsonObject.get("content"), Message.class);
    }
}

package com.sergeymild.chat.convertors;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import com.sergeymild.chat.models.ErrorState;
import retrofit.RetrofitError;

/**
 * Created by sergeyMild on 20/11/15.
 */
public class ErrorConvertor {
    private RetrofitError error;

    public ErrorConvertor(RetrofitError error) {
        this.error = error;
    }

    public ErrorState parse() {
        if (error == null) {
            return new ErrorState("Unknown error", -1);
        }

        if (error.getMessage() != null && error.getResponse() != null) {
            return new ErrorState(error.getMessage(), error.getResponse().getStatus());
        }

        if (error.getMessage() != null && error.getResponse() == null) {
            return new ErrorState(error.getMessage(), -2);
        }

        if (error.getResponse() == null) {
            return new ErrorState("Unknown error", -1);
        }

        if (error.getResponse().getStatus() == HttpURLConnection.HTTP_INTERNAL_ERROR) {
            return new ErrorState("HTTP BAD GATEWAY", HttpURLConnection.HTTP_INTERNAL_ERROR);
        }

        if (error.getBody() == null) {
            return new ErrorState(error.getMessage(), -1);
        }

        try {
            InputStream in = error.getResponse().getBody().in();
            BufferedReader bReader = new BufferedReader(new InputStreamReader(in));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            JsonElement jsonElement = new JsonParser().parse(stringBuilder.toString());

            return new Gson().fromJson(jsonElement.getAsJsonObject().get("error"), ErrorState.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ErrorState("Unknown error", -1);
    }
}

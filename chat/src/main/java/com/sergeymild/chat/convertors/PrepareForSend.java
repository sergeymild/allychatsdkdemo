package com.sergeymild.chat.convertors;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sergeymild.chat.models.Message;

/**
 * Created by sergeyMild on 21/11/15.
 */
public class PrepareForSend {
    @CheckResult @NonNull @WorkerThread
    public String build(Message message) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("type", "message");
        jsonObject.add("content", new Gson().toJsonTree(message));
        return jsonObject.toString();
    }
}

package com.sergeymild.chat.models;

import android.support.annotation.IntDef;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by sergeyMild on 20/11/15.
 */
@IntDef({MessageStatus.NO_STATUS, MessageStatus.STATUS_NEW, MessageStatus.STATUS_SENDING, MessageStatus.STATUS_RESENDING, MessageStatus.STATUS_SEND, MessageStatus.STATUS_FAILED})
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MessageStatus {
    String[] names = {"NEW", "SENDING", "RESENDING", "SEND", "FAILED"};
    int NO_STATUS = -1;
    int STATUS_NEW = 0;
    int STATUS_SENDING = 1;
    int STATUS_RESENDING = 2;
    int STATUS_SEND = 3;
    int STATUS_FAILED = 4;
}

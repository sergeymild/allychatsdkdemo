package com.sergeymild.chat.models;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.sergeymild.chat.utils.Logger;
import com.sergeymild.chat.utils.StringUtils;

/**
 * Created by sergeyMild on 13/11/15.
 */
@SuppressWarnings("unused")
public final class Room {
    public static class Fields {
        public static final String ROOM_ID = "roomId";
    }

    @SerializedName("last_message")
    private Message lastMessage;

    private Integer unreadMessageCount;

    @SerializedName("is_support")
    private boolean isSupport;

    @SerializedName("id")
    private String id;

    @SerializedName("users")
    private UserId[] users;

    @SerializedName("last_read_message_id")
    private String lastReadMessageId;

    @CheckResult
    public final Integer getUnreadMessageCount() {
        return unreadMessageCount;
    }

    @CheckResult
    public final Message getLastMessage() {
        return lastMessage;
    }

    @CheckResult
    public final boolean isSupport() {
        return isSupport;
    }

    @CheckResult
    public final String getId() {
        return id;
    }

    @CheckResult
    public final UserId[] getUsers() {
        return users;
    }

    @CheckResult
    public final String getLastReadMessageId() {
        return lastReadMessageId;
    }

    public void setUnreadMessageCount(@NonNull final Integer unreadMessageCount) {
        this.unreadMessageCount = unreadMessageCount;
    }

    static class UserId {
        @SerializedName("id")
        private String id;
    }

    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}

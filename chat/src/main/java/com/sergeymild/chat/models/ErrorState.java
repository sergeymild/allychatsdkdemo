package com.sergeymild.chat.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sergeyMild on 20/11/15.
 */
public final class ErrorState {
    @SerializedName("message")
    private String message;

    @SerializedName("code")
    private int code;

    public ErrorState() {
    }

    public ErrorState(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "ErrorState{" +
                "message='" + message + '\'' +
                ", code=" + code +
                '}';
    }
}

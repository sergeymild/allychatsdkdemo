package com.sergeymild.chat.models;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.sergeymild.chat.utils.Logger;
import com.sergeymild.chat.utils.StringUtils;

/**
 * Created by sergeyMild on 13/11/15.
 */
public final class Sender {
    @SerializedName("alias")
    private String alias;

    @Nullable @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public Sender() {
    }

    public Sender(@NonNull final String id) {
        this.id = id;
    }

    @CheckResult
    public final String getAlias() {
        return alias;
    }

    @Nullable @CheckResult
    public final String getAvatarUrl() {
        return avatarUrl;
    }

    @CheckResult
    public final String getId() {
        return id;
    }

    @CheckResult
    public final String getName() {
        return name;
    }

    @Override @CheckResult
    public final String toString() {
        return StringUtils.toString(this);
    }


    @CheckResult
    public final boolean isAvatarEmpty() {
        return "NONE".equals(getAvatarUrl()) && TextUtils.isEmpty(getAvatarUrl());
    }
}

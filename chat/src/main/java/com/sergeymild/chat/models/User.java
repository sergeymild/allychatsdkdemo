package com.sergeymild.chat.models;

import android.support.annotation.CheckResult;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;

/**
 * Created by sergeyMild on 12/11/15.
 */
public final class User {
    @SerializedName("geolocation")
    private String geolocation;

    @SerializedName("is_operator")
    private boolean isOperator;

    @SerializedName("device_type")
    private String deviceType;

    @SerializedName("os_version")
    private String osVersion;

    @SerializedName("tags")
    private String[] tags;

    @SerializedName("apps")
    private String[] apps;

    @SerializedName("name")
    private String name;

    @SerializedName("alias")
    private String alias;

    @SerializedName("avatar_url")
    private String avatarUrl;

    @SerializedName("is_admin")
    private boolean isAdmin;

    @SerializedName("is_observer")
    private boolean isObserver;

    @SerializedName("is_temporary")
    private boolean isTemporary;

    @SerializedName("login")
    private String login;

    @SerializedName("app_version")
    private String appVersion;

    @SerializedName("id")
    private String id;

    @SerializedName("tzoffset")
    private String tzoffset;

    @CheckResult
    public final String getId() {
        return id;
    }

    @CheckResult
    public final String getAlias() {
        return alias;
    }

    @CheckResult
    public final boolean isValid() {
        return !TextUtils.isEmpty(id);
    }

    @Override
    public String toString() {
        return "User{" +
                "geolocation='" + geolocation + '\'' +
                ", isOperator=" + isOperator +
                ", deviceType='" + deviceType + '\'' +
                ", osVersion='" + osVersion + '\'' +
                ", tags=" + Arrays.toString(tags) +
                ", apps=" + Arrays.toString(apps) +
                ", name='" + name + '\'' +
                ", alias='" + alias + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", avatarUrl='" + avatarUrl + '\'' +
                ", isAdmin=" + isAdmin +
                ", isObserver=" + isObserver +
                ", isTemporary=" + isTemporary +
                ", login='" + login + '\'' +
                ", appVersion='" + appVersion + '\'' +
                ", id='" + id + '\'' +
                ", tzoffset='" + tzoffset + '\'' +
                '}';
    }
}

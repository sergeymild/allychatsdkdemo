package com.sergeymild.chat.models;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import com.sergeymild.chat.utils.Logger;
import com.sergeymild.chat.utils.AllyChatPreferences;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by sergeyMild on 26/11/15.
 */
public final class SdkSettings {
    @Nullable
    private String alias;
    private String host;
    private String appId;
    private boolean isLoggingEnabled = true;
    private AtomicBoolean isInitialized = new AtomicBoolean(false);
    private AtomicBoolean isInProcessInitializing = new AtomicBoolean(false);

    private static class InstanceHolder {
        private static final SdkSettings INSTANCE = new SdkSettings();
    }

    public static SdkSettings getInstance() {
        return InstanceHolder.INSTANCE;
    }


    @NonNull @CheckResult
    public final String getHost() {
        return host;
    }

    public final void setHost(@NonNull final String host) {
        this.host = "https://" + host.replace("http://", "").replace("https://", "");;
    }

    @Nullable @CheckResult
    public final String getAlias() {
        return alias;
    }

    public final void setAlias(@Nullable final String alias) {
        if (!TextUtils.isEmpty(alias)) {
            AllyChatPreferences.setAlias(alias);
            this.alias = alias;
            return;
        }

        String prefAlias = AllyChatPreferences.getAlias();
        if (!TextUtils.isEmpty(prefAlias)) {
            this.alias = prefAlias;
            return;
        }

        this.alias = String.format("allychat_anonym_client_%s", UUID.randomUUID().toString().replace("-", ""));
        Logger.logi("alias was null, generate new anonym user", this.alias);
        AllyChatPreferences.setAlias(this.alias);
    }

    @NonNull @CheckResult
    public final String getAppId() {
        return appId;
    }

    public final void setAppId(@NonNull final String appId) {
        this.appId = appId;
    }

    public final boolean isLoggingEnabled() {
        return isLoggingEnabled;
    }

    public final void setIsLoggingEnabled(final boolean isLoggingEnabled) {
        this.isLoggingEnabled = isLoggingEnabled;
    }

    public final boolean isInitialized() {
        return isInitialized.get();
    }

    public final boolean isInProcessInitializing() {
        return isInProcessInitializing.get();
    }

    public final void setIsInitialized(final boolean isInitialized) {
        this.isInitialized.set(isInitialized);
    }

    public final void setIsInProcessInitializing(final boolean isInProcessInitializing) {
        this.isInProcessInitializing.set(isInProcessInitializing);
    }
}

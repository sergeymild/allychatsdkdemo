package com.sergeymild.chat.models;

import android.support.annotation.CheckResult;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sergeyMild on 22/11/15.
 */
public class MessageFile {
    @SerializedName("file")
    private String url;

    public MessageFile() {
    }

    public MessageFile(final String url) {
        this.url = url;
    }

    @CheckResult
    public final String getUrl() {
        return url;
    }

    public final void setUrl(final String url) {
        this.url = url;
    }
}

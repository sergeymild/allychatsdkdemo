package com.sergeymild.chat.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.CheckResult;

import com.google.gson.annotations.SerializedName;
import com.sergeymild.chat.utils.StringUtils;

/**
 * Created by sergeyMild on 07/12/15.
 */


public final class PushModel implements Parcelable {
    @SerializedName("alert")
    private String message;

    @SerializedName("origin")
    private String origin;

    @SerializedName("push_hash")
    private String pushHash;

    @SerializedName("sound")
    private String sound;

    @SerializedName("unread_count")
    private int unreadCount;


    @CheckResult
    public final String getMessage() {
        return message;
    }

    @CheckResult
    public final String getOrigin() {
        return origin;
    }

    @CheckResult
    public final String getPushHash() {
        return pushHash;
    }

    @CheckResult
    public final String getSound() {
        return sound;
    }

    @CheckResult
    public final int getUnreadCount() {
        return unreadCount;
    }

    public int describeContents() {
        return 0;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeString(this.origin);
        dest.writeString(this.pushHash);
        dest.writeString(this.sound);
        dest.writeInt(this.unreadCount);
    }

    public PushModel() {
    }

    private PushModel(Parcel in) {
        this.message = in.readString();
        this.origin = in.readString();
        this.pushHash = in.readString();
        this.sound = in.readString();
        this.unreadCount = in.readInt();
    }

    public static final Creator<PushModel> CREATOR = new Creator<PushModel>() {
        public PushModel createFromParcel(Parcel source) {
            return new PushModel(source);
        }

        public PushModel[] newArray(int size) {
            return new PushModel[size];
        }
    };


    @Override
    public String toString() {
        return StringUtils.toString(this);
    }
}

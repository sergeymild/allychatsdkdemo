package com.sergeymild.chat.models;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.utils.Checks;
import com.sergeymild.chat.utils.Logger;
import com.sergeymild.chat.utils.StringUtils;

import java.util.UUID;

/**
 * Created by sergeyMild on 12/11/15.
 */
public final class Message {
    @SerializedName("created_at_millis")
    private long createdAtMillis;

    @SerializedName("read")
    private boolean read;

    @Nullable @SerializedName("file")
    private String file;

    @Nullable @SerializedName("is_hidden")
    private Boolean isHidden;

    @Nullable @SerializedName("event")
    private String event;

    @SerializedName("room")
    private String room;

    @SerializedName("sender")
    private Sender user;

    @SerializedName("created_at")
    private double createdAt;

    @Nullable @SerializedName("message")
    private String message;

    @Nullable @SerializedName("client_id")
    private String localId;

    @SerializedName("id")
    private String id;

    @SerializedName("issue")
    private String issue;

    @MessageStatus
    @Expose(serialize = false, deserialize = false)
    private int status = MessageStatus.STATUS_SEND;

    @Expose(serialize = false, deserialize = false)
    private boolean fromMe;

    public final void setCreatedAtMillis(long createdAtMillis) {
        this.createdAtMillis = createdAtMillis;
    }

    private void setMessage(@NonNull final String message) {
        this.message = message;
    }

    private void setUser(final Sender user) {
        this.user = user;
    }

    private void setRoom(@NonNull final String room) {
        this.room = room;
    }

    public void setStatus(@MessageStatus final int status) {
        this.status = status;
    }

    public void setFromMe(final boolean fromMe) {
        this.fromMe = fromMe;
    }

    private void setLocalId(@NonNull final String localId) {
        this.localId = localId;
    }

    @CheckResult
    public final long getCreatedAtMillis() {
        return createdAtMillis;
    }

    public final void setFile(@Nullable final String file) {
        this.file = file;
    }

    @Nullable @CheckResult
    public final String getLocalId() {
        return localId;
    }

    @CheckResult
    public final boolean isRead() {
        return read;
    }

    @Nullable @CheckResult
    public final String getFile() {
        return file;
    }

    @Nullable @CheckResult
    public final Boolean isHidden() {
        return isHidden;
    }

    @Nullable @CheckResult
    public final String getEvent() {
        return event;
    }

    @CheckResult
    public final String getRoom() {
        return room;
    }

    @CheckResult
    public final Sender getUser() {
        return user;
    }

    @CheckResult
    public final double getCreatedAt() {
        return createdAt;
    }

    @Nullable @CheckResult
    public final String getMessage() {
        return message;
    }

    @CheckResult
    public final String getIssue() {
        return issue;
    }

    @CheckResult
    public final String getId() {
        return id;
    }

    @MessageStatus @CheckResult
    public final int getStatus() {
        return status;
    }

    @CheckResult
    public final String getNameStatus() {
        if (status == MessageStatus.NO_STATUS) {
            return MessageStatus.names[3];
        } else {
            return MessageStatus.names[status];
        }
    }

    @CheckResult
    public final boolean isFromMe() {
        return fromMe || (getUser() != null && getUser().getId() != null && getUser().getId().equals(ChatUtils.userId()));
    }

    @CheckResult
    public final boolean isEmpty() {
        return TextUtils.isEmpty(getMessage());
    }


    public final void updateFrom(@NonNull final Message message) {
        createdAtMillis = message.createdAtMillis;
        read = message.read;
        file = message.file;
        isHidden = message.isHidden;
        event = message.event;
        room = message.room;
        user = message.user;
        createdAt = message.createdAt;
        this.message = message.message;
        localId = message.localId;
        id = message.id;
        issue = message.issue;
    }


    @Override
    public String toString() {
        return StringUtils.toString(this);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message1 = (Message) o;

        if (createdAtMillis != message1.createdAtMillis) return false;
        if (!room.equals(message1.room)) return false;
        if (!user.equals(message1.user)) return false;
        return localId.equals(message1.localId);

    }

    @Override
    public int hashCode() {
        int result;
        result = (int) (createdAtMillis ^ (createdAtMillis >>> 32));
        result = 31 * result + room.hashCode();
        result = 31 * result + user.hashCode();
        return result;
    }

    public void setIsRead(boolean isRead) {
        this.read = isRead;
    }

    public static class Builder {
        private String message;
        private String roomId;
        private String filePath;
        private long createdAt = -1;

        public Builder setMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
            return this;
        }

        public Builder setRoom(String roomId) {
            this.roomId = roomId;
            return this;
        }

        public Builder setFilePath(String filePath) {
            this.filePath = filePath;
            return this;
        }

        public Message build() {
            Checks.throwIfAppNotInitialized();
            Message messageModel = new Message();
            messageModel.setMessage(message);
            messageModel.setCreatedAtMillis(createdAt != -1 ? createdAt : System.currentTimeMillis());
            messageModel.setRoom(roomId);
            messageModel.setUser(new Sender(ChatUtils.userId()));
            messageModel.setFromMe(true);
            messageModel.setIsRead(true);
            if (!TextUtils.isEmpty(filePath)) messageModel.setFile(filePath);
            messageModel.setLocalId(UUID.randomUUID().toString().replace("-", ""));
            return messageModel;
        }
    }
}

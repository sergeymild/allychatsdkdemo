package com.sergeymild.chat.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by sergeyMild on 12/11/15.
 */
public final class TokenResponse {
    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "TokenResponse{" +
                "token='" + token + '\'' +
                '}';
    }
}

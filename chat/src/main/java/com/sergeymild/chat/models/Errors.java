package com.sergeymild.chat.models;

/**
 * Created by sergeyMild on 27/11/15.
 */
public enum Errors {
    CHAT_IS_NOT_CONNECTED(-100, "Chat is not connected"),
    FAILED_TO_SENDING_MESSAGE(-101, "Failed to sending message"),
    FAILED_TO_UPLOAD_FILE(-102, "Failed to upload file"),
    FAILED_TO_GET_TOKEN(-103, "Failed to get token"),
    FAILED_TO_REGISTER_GCM(-104, "Failed to register gcm"),
    FAILED_APP_IN_NOT_INITIALIZED(-105, "Failed app is not initialized"),
    USER_IS_NOT_REGISTERED(-106, "User is not registered");

    public String message;
    public int status;

    Errors(int status, String message) {
        this.status = status;
        this.message = message;
    }
}

package com.sergeymild.chat.models;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import com.sergeymild.chat.AllyChat;
import com.sergeymild.chat.utils.ContextUtils;

import java.util.TimeZone;

/**
 * Created by sergeyMild on 12/11/15.
 */
public final class ChatParameters {
    @NonNull
    private final Context context;

    public ChatParameters(@NonNull final Context context) {
        this.context = context;
    }

    @NonNull @CheckResult
    public final String getAppVersion() {
        return ContextUtils.getAppVersion(context);
    }

    @NonNull @CheckResult
    public final String getAppBuild() {
        return String.valueOf(ContextUtils.getAppVersionCode(context));
    }

    @NonNull @CheckResult
    public final String getBundleID() {
        return ContextUtils.getBundleID(context);
    }

    @NonNull @CheckResult
    public final TimeZone getTimeZone() {
        return TimeZone.getDefault();
    }

    @NonNull @CheckResult
    public final String getAndroidId() {
        return Settings.Secure.getString(AllyChat.getInstance().getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @NonNull @CheckResult
    public final String getMachineName() {
        String machineName = System.getProperty("os.version");
        if (machineName == null) machineName = "Unknown";
        return String.format("%s/%s/%s", Build.MANUFACTURER, Build.MODEL, machineName);
    }

    @NonNull @CheckResult
    public final String getOSVersion() {
        return "Android/" + Build.VERSION.RELEASE;
    }
}

package com.sergeymild.chat;

import android.content.Context;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.sergeymild.chat.callbacks.OnFailureInitialize;
import com.sergeymild.chat.callbacks.OnSuccessInitialize;
import com.sergeymild.chat.models.ChatParameters;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.SdkSettings;
import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.models.User;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.utils.AllyChatPreferences;
import com.sergeymild.chat.utils.Checks;
import com.sergeymild.chat.utils.ConnectionManager;
import com.sergeymild.chat.utils.DispatchUtil;
import com.sergeymild.chat.models.Errors;
import com.sergeymild.chat.utils.Logger;

import retrofit.RetrofitError;


/**
 * Created by sergeyMild on 12/11/15.
 */
public class AllyChat {
    private ChatParameters chatParameters;
    private TokenResponse token;
    private User user;
    private OnSuccessInitialize onSuccessInitialize;
    private OnFailureInitialize onFailureInitialize;
    private Context context;

    private AllyChat() {}

    private static class InstanceHolder {
        private static final AllyChat INSTANCE = new AllyChat();
    }

    public static AllyChat getInstance() {
        return InstanceHolder.INSTANCE;
    }


    private void init(@NonNull final Context context, @Nullable final OnSuccessInitialize onSuccessInitialize, @Nullable final OnFailureInitialize onFailureInitialize) {
        this.context = context;
        ConnectionManager.context = this.context;

        if (!Checks.checkOnline(onFailureInitialize)) return;

        DispatchUtil.getInstance().backgroundQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                setChatParameters(new ChatParameters(context.getApplicationContext()));
                setOnSuccessInitialize(onSuccessInitialize);
                setOnFailureInitialize(onFailureInitialize);
                setToken();
            }
        });

    }

    public final void setChatParameters(@NonNull final ChatParameters chatParameters) {
        this.chatParameters = chatParameters;
    }

    public final void setOnSuccessInitialize(@Nullable final OnSuccessInitialize onSuccessInitialize) {
        this.onSuccessInitialize = onSuccessInitialize;
    }

    public final void setOnFailureInitialize(@Nullable OnFailureInitialize onFailureInitialize) {
        this.onFailureInitialize = onFailureInitialize;
    }

    public final boolean isLoggingEnabled() {
        return SdkSettings.getInstance().isLoggingEnabled();
    }

    public final boolean isInitialized() {
        return SdkSettings.getInstance().isInitialized();
    }

    public final boolean isInProcessInitializing() {
        return SdkSettings.getInstance().isInProcessInitializing();
    }

    public final void setIsInitialized(final boolean isInitialized) {
        SdkSettings.getInstance().setIsInitialized(isInitialized);
    }

    public final void setIsInProcessInitializing(final boolean isInProcessInitializing) {
        SdkSettings.getInstance().setIsInProcessInitializing(isInProcessInitializing);
    }

    @NonNull
    public final Context getContext() {
        return context;
    }

    @NonNull
    public final String getHost() {
        return SdkSettings.getInstance().getHost();
    }

    @Nullable
    public final User getUser() {
        return user;
    }

    public final void clear() {
        token = null;
        user = null;

        String gcmToken = AllyChatPreferences.getGcmToken();
        if (gcmToken != null) ChatUtils.unRegisterGcm(gcmToken);

        SdkSettings.getInstance().setIsInitialized(false);
        AllyChatPreferences.clear();
    }

    @CheckResult
    public final String getAppId() {
        return SdkSettings.getInstance().getAppId();
    }

    @CheckResult
    public final String getAlias() {
        return SdkSettings.getInstance().getAlias();
    }

    @CheckResult
    public final TokenResponse tokenResponse() {
        return token;
    }


    public final void setToken(@NonNull final TokenResponse token) {
        this.token = token;
    }

    @CheckResult
    public final String getAndroidId() {
        return chatParameters.getAndroidId();
    }


    private void setToken() {
        Logger.logi("AllyChat trying token");
        if (!Checks.checkOnline())return;

        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                SdkSettings sdkSettings = SdkSettings.getInstance();
                try {
                    user = ChatUtils.registerSync(sdkSettings.getAlias(), chatParameters);
                    AllyChatPreferences.setUser(AllyChat.this.user);
                    token = ChatUtils.getToken(sdkSettings.getAlias(), sdkSettings.getAppId());
                    Logger.logi("token: ", token.getToken());
                    onSuccess();

                } catch (final RetrofitError error) {
                    DispatchUtil.getInstance().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Logger.logi("AllyChat get token fail");
                            Logger.loge(error);
                            if (onFailureInitialize != null)
                                onFailureInitialize.onFailInitialize(new ErrorState(Errors.FAILED_TO_GET_TOKEN.message, Errors.FAILED_TO_GET_TOKEN.status));
                        }
                    });
                }
            }
        });
    }

    private void onSuccess() {
        SdkSettings.getInstance().setIsInitialized(true);
        DispatchUtil.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Logger.logi("AllyChat get token success");
                if (onSuccessInitialize != null)
                    onSuccessInitialize.onSuccess(AllyChat.this);
            }
        });
    }

    public final static class Builder {
        private Context context;
        private String host;
        private String alias;
        private String appId;
        private OnSuccessInitialize onSuccessInitialize;
        private OnFailureInitialize onFailureInitialize;
        private boolean isLoggingEnabled = true;

        public Builder setContext(@NonNull final Context context) {
            this.context = context;
            return this;
        }

        public Builder setHost(@NonNull final String host) {
            this.host = host;
            return this;
        }

        public Builder setAlias(@NonNull final String alias) {
            this.alias = alias;
            return this;
        }

        public Builder setAppId(@NonNull final String appId) {
            this.appId = appId;
            return this;
        }

        public Builder setIsLoggingEnabled(final boolean isLoggingEnabled) {
            this.isLoggingEnabled = isLoggingEnabled;
            return this;
        }

        public Builder setOnFailureInitialize(@NonNull final OnFailureInitialize onFailureInitialize) {
            this.onFailureInitialize = onFailureInitialize;
            return this;
        }

        public Builder setOnSuccessInitialize(@NonNull final OnSuccessInitialize onSuccessInitialize) {
            this.onSuccessInitialize = onSuccessInitialize;
            return this;
        }

        public void build() {
            Logger.logi("AllyChat Builder building AllychatSdk");
            AllyChatPreferences.init(context);
            SdkSettings sdkSettings = SdkSettings.getInstance();


            Checks.checkNotEmpty(appId, context.getString(R.string.app_id_required));
            sdkSettings.setAppId(appId);

            Checks.checkNotEmpty(host, context.getString(R.string.host_required));
            sdkSettings.setHost(host);
            sdkSettings.setAlias(alias);

            sdkSettings.setIsLoggingEnabled(isLoggingEnabled);
            AllyChat.getInstance().init(context.getApplicationContext(), onSuccessInitialize, onFailureInitialize);
        }
    }
}

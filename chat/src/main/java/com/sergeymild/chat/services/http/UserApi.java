package com.sergeymild.chat.services.http;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.util.Date;
import java.util.TimeZone;

import com.sergeymild.chat.AllyChat;
import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.models.ChatParameters;
import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.models.User;
import com.sergeymild.chat.utils.Logger;
import retrofit.RestAdapter;

/**
 * Created by sergeyMild on 13/11/15.
 */
final class UserApi implements IUserApi {
    private RestAdapter adapter;
    private UserService userService;

    private static class InstanceHolder {
        private static final UserApi INSTANCE = new UserApi();
    }

    public static UserApi getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private UserApi() {
        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(AllyChat.getInstance().getHost());

        if (AllyChat.getInstance().isLoggingEnabled()) {
            builder.setLogLevel(RestAdapter.LogLevel.FULL);
        }

        adapter = builder.build();

        userService = adapter.create(UserService.class);
    }


    @Override
    public void register(@NonNull String alias, @NonNull ChatParameters chatParameters, ChatCallback<User> chatCallback) {
        Logger.logi("UserApi register user");
        String appVersion = chatParameters.getAppVersion();
        String appBuild = chatParameters.getAppBuild();
        String bundleID = chatParameters.getBundleID();
        TimeZone currentTimeZone = chatParameters.getTimeZone();
        userService.register(alias, chatParameters.getOSVersion(), chatParameters.getMachineName(),
                String.format("%s/%s.%s", bundleID, appVersion, appBuild),
                String.valueOf(currentTimeZone.getOffset(new Date().getTime()) / 1000), chatCallback);
    }

    @Override @WorkerThread
    public User registerSync(@NonNull String alias, @NonNull ChatParameters chatParameters) {
        Logger.logi("UserApi register user sync");
        String appVersion = chatParameters.getAppVersion();
        String appBuild = chatParameters.getAppBuild();
        String bundleID = chatParameters.getBundleID();
        TimeZone currentTimeZone = chatParameters.getTimeZone();
        return userService.registerSync(alias, chatParameters.getOSVersion(), chatParameters.getMachineName(),
                String.format("%s/%s.%s", bundleID, appVersion, appBuild),
                String.valueOf(currentTimeZone.getOffset(new Date().getTime()) / 1000));
    }

    @Override @WorkerThread
    public TokenResponse getToken(@NonNull String alias, @NonNull String appId) {
        Logger.logi("UserApi get token");
        return userService.getToken(alias, appId);
    }
}

package com.sergeymild.chat.services.web_socket.api;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.sergeymild.chat.models.Message;

/**
 * Created by sergeyMild on 20/11/15.
 */
public interface WSChatApi {
    void connect();
    void disconnect();
    boolean isConnected();

    @WorkerThread
    void sendMessage(@NonNull Message message);
}

package com.sergeymild.chat.services.http;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.util.List;

import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.MessageFile;
import com.sergeymild.chat.models.Room;
import com.sergeymild.chat.utils.Logger;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by sergeyMild on 12/11/15.
 */
final class ChatApi implements IChatApi {
    private MessagesApiService messagesApiService;
    private RoomsService roomsService;

    protected static class InstanceHolder {
        private static final ChatApi INSTANCE = new ChatApi();
    }

    public static ChatApi getInstance() {
        return InstanceHolder.INSTANCE;
    }

    private ChatApi() {
        messagesApiService = RestAdapter.getInstance().getAdapter().create(MessagesApiService.class);
        roomsService = RestAdapter.getInstance().getAdapter().create(RoomsService.class);
    }


    @Override @WorkerThread
    public List<Room> getRooms() {
        return roomsService.getRooms();
    }

    @Override @WorkerThread
    public Room getSupportRoom() {
        return roomsService.getSupportRoom();
    }

    /*
    *
    *   GET Messages
    *
    * */

    @Override @WorkerThread
    public List<Message> getMessages(@NonNull final String roomId, final boolean stackFromEnd, final int limit) {
        return messagesApiService.getRoomMessages(roomId, stackFromEndString(stackFromEnd), limit);
    }

    @Override @WorkerThread
    public List<Message> getMoreMessages(@NonNull final String roomId, final boolean stackFromEnd, final int limit, final int offset) {
        return messagesApiService.getMoreRoomMessages(roomId, stackFromEndString(stackFromEnd), limit, offset);
    }

    @Override
    public List<Message> getMoreByLastMessageIdMessages(@NonNull final String roomId, final boolean stackFromEnd, final int limit, @NonNull final String lastMessageId) {
        return messagesApiService.getMoreByLastMessageIdMessages(roomId, stackFromEndString(stackFromEnd), limit, lastMessageId);
    }

    @Override
    public void readMessage(@NonNull final String messageId) {
        messagesApiService.readMessage(messageId, "");
    }

    @Override @WorkerThread
    public Response getUnreadCount(@NonNull final String roomId) {
        return roomsService.getUnreadCount(roomId);
    }



    /*
    *
    *   Upload file
    *
    * */

    @Override @WorkerThread
    public MessageFile uploadFile(@NonNull final TypedFile typedFile) {
        Logger.logi("uploadFile " + typedFile);
        return messagesApiService.uploadFile(typedFile);
    }


    /*
    *
    *   Utils methods
    *
    * */

    private String stackFromEndString(final boolean isStackFromBottom) {
        return isStackFromBottom ? "old" : "new";
    }
}

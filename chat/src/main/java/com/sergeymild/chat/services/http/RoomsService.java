package com.sergeymild.chat.services.http;

import com.sergeymild.chat.models.Room;

import java.util.List;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by sergeyMild on 26/11/15.
 */
interface RoomsService {

    @GET("/api/2/rooms")
    List<Room> getRooms();

    @GET("/api/2/room/support")
    Room getSupportRoom();

    @GET("/api/2/room/{roomId}/messages?unread=1&limit=0")
    Response getUnreadCount(@Path("roomId") String roomId);
}

package com.sergeymild.chat.services.http;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import retrofit.client.Response;

/**
 * Created by sergeyMild on 03/12/15.
 */
final class GcmApi implements IGcmApi {
    private GcmService gcmService;

    public GcmApi() {
        gcmService = RestAdapter.getInstance().getAdapter().create(GcmService.class);
    }

    @WorkerThread @Override
    public Response registerGcm(@NonNull final String deviceId, @NonNull final String deviceToken) {
       return gcmService.registerGcm(deviceId, "android", deviceToken);
    }

    @WorkerThread @Override
    public Response unRegisterGcm(@NonNull final String deviceId) {
        return gcmService.unRegisterGcm(deviceId);
    }
}

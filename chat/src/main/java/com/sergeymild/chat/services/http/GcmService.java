package com.sergeymild.chat.services.http;

import retrofit.client.Response;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by sergeyMild on 03/12/15.
 */
interface GcmService {
    @POST("/api/devices") @FormUrlEncoded
    Response registerGcm(@Field("device_id") String deviceId, @Field("device_platform") String platform, @Field("device_token") String deviceToken);

    @DELETE("/api/devices")
    Response unRegisterGcm(@Query("device_id") String deviceId);
}

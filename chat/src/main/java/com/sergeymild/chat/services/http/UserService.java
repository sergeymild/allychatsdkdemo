package com.sergeymild.chat.services.http;



import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.models.User;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;

import java.util.List;

/**
 * Created by sergeyMild on 12/11/15.
 */
interface UserService {
    @POST("/api/token") @FormUrlEncoded
    TokenResponse getToken(@Field("alias") String alias, @Field("app_id") String appId);

    @POST("/api/user/register") @FormUrlEncoded
    void register(@Field("alias") String alias,
                  @Field("os_version") String osVersion,
                  @Field("device_type") String deviceType,
                  @Field("app_version") String appVersion,
                  @Field("tzoffset") String tzoffset, Callback<User> callback);

    @POST("/api/user/register") @FormUrlEncoded
    User registerSync(@Field("alias") String alias,
                      @Field("os_version") String osVersion,
                      @Field("device_type") String deviceType,
                      @Field("app_version") String appVersion,
                      @Field("tzoffset") String tzoffset);

    @GET("/api/2/me")
    User getMe();

    @GET("/api/2/users")
    List<User> getUsers();
}

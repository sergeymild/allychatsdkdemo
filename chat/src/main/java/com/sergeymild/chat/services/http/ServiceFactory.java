package com.sergeymild.chat.services.http;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.sergeymild.chat.services.web_socket.api.WebSocketChat;
import com.sergeymild.chat.event_publisher.ChatEventEmitter;
import com.sergeymild.chat.event_publisher.IChatEventEmitter;
import com.sergeymild.chat.services.web_socket.api.WSChatApi;

/**
 * Created by sergeyMild on 14/01/16.
 */
public final class ServiceFactory {
    @CheckResult @NonNull
    public static IChatEventEmitter getChatEventEmitter() {
        return ChatEventEmitter.getInstance();
    }

    @CheckResult @NonNull
    public static IUserApi getUserApi() {
        return UserApi.getInstance();
    }

    @CheckResult @NonNull
    public static WSChatApi getWsChatApi() {
        return WebSocketChat.getInstance();
    }

    @CheckResult @NonNull
    public static IChatApi getChatApi() {
        return ChatApi.getInstance();
    }

    @CheckResult @NonNull
    public static IGcmApi getGcmApi() {
        return new GcmApi();
    }
}

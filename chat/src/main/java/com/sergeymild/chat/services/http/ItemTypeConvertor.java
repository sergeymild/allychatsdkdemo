package com.sergeymild.chat.services.http;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by sergeyMild on 13/11/15.
 * Package Private
 */
final class ItemTypeConvertor implements TypeAdapterFactory {

    public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {

        final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
        final TypeAdapter<JsonElement> elementAdapter = gson.getAdapter(JsonElement.class);

        return new TypeAdapter<T>() {

            public void write(JsonWriter out, T value) throws IOException {
                delegate.write(out, value);
            }

            public T read(JsonReader in) throws IOException {

                JsonElement jsonElement = elementAdapter.read(in);

                if (jsonElement.isJsonObject()) {
                    JsonObject jsonObject = jsonElement.getAsJsonObject();
                    if (jsonObject.has("rooms") && jsonObject.get("rooms").isJsonArray()) {
                        jsonElement = jsonObject.get("rooms").getAsJsonArray();
                    }

                    if (jsonObject.has("messages") && jsonObject.get("messages").isJsonArray()) {
                        jsonElement = jsonObject.get("messages").getAsJsonArray();
                    }
                }
                return delegate.fromJsonTree(jsonElement);
            }
        }.nullSafe();
    }
}

package com.sergeymild.chat.services.http;

import java.util.List;

import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.MessageFile;
import com.sergeymild.chat.models.Room;
import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by sergeyMild on 12/11/15.
 */
interface MessagesApiService {

    @PUT("/api/2/message/{messageId}/read")
    Response readMessage(@Path("messageId") String messageId, @Body String body);

    @GET("/api/2/room/{roomId}/messages")
    List<Message> getRoomMessages(@Path("roomId") String roomId, @Query("show") String show, @Query("limit") int limit);

    @GET("/api/2/room/{roomId}/messages")
    List<Message> getMoreRoomMessages(@Path("roomId") String roomId, @Query("show") String show, @Query("limit") int limit, @Query("offset") int offset);

    @GET("/api/2/room/{roomId}/messages")
    List<Message> getMoreByLastMessageIdMessages(@Path("roomId") String roomId, @Query("show") String show, @Query("limit") int limit, @Query("last_read_message") String lastMessageId);


    @POST("/api/2/upload") @Multipart
    MessageFile uploadFile(@Part("file") TypedFile file);
}

package com.sergeymild.chat.services.http;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import retrofit.client.Response;

/**
 * Created by sergeyMild on 14/01/16.
 */
public interface IGcmApi {
    @WorkerThread
    Response registerGcm(@NonNull final String deviceId, @NonNull final String deviceToken);

    @WorkerThread
    Response unRegisterGcm(@NonNull final String deviceId);
}

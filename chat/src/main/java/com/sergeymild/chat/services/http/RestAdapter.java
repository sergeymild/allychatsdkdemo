package com.sergeymild.chat.services.http;

import com.google.gson.GsonBuilder;
import com.sergeymild.chat.AllyChat;

import retrofit.RequestInterceptor;
import retrofit.converter.GsonConverter;

/**
 * Created by sergeyMild on 17/11/15.
 */
final class RestAdapter {
    private retrofit.RestAdapter adapter;
    private RequestInterceptor tokenHeader = new RequestInterceptor() {
        @Override public void intercept(RequestFacade request) {
            request.addHeader("Authorization", "Token " + AllyChat.getInstance().tokenResponse().getToken());
        }
    };

    private static class InstanceHolder {
        private static final RestAdapter INSTANCE = new RestAdapter();
    }

    public static RestAdapter getInstance() {
        return InstanceHolder.INSTANCE;
    }

    public RestAdapter() {
        retrofit.RestAdapter.Builder builder = new retrofit.RestAdapter.Builder().setConverter(getConverter())
                .setEndpoint(AllyChat.getInstance().getHost());

        if (AllyChat.getInstance().isLoggingEnabled()) {
            builder.setLogLevel(retrofit.RestAdapter.LogLevel.FULL);
        }

        if (AllyChat.getInstance().tokenResponse() != null) {
            builder.setRequestInterceptor(tokenHeader);
        }

        adapter = builder.build();
    }

    private GsonConverter getConverter() {
        return new GsonConverter(new GsonBuilder()
                .registerTypeAdapterFactory(new ItemTypeConvertor()).create());
    }


    public retrofit.RestAdapter getAdapter() {
        return adapter;
    }
}

package com.sergeymild.chat.services.web_socket.api;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import com.sergeymild.chat.AllyChat;
import com.sergeymild.chat.controllers.MessageController;
import com.sergeymild.chat.convertors.ParseFromString;
import com.sergeymild.chat.convertors.PrepareForSend;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.Errors;
import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.MessageStatus;
import com.sergeymild.chat.models.SdkSettings;
import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.services.http.ChatUtils;
import com.sergeymild.chat.services.web_socket.library.WebSocket;
import com.sergeymild.chat.services.web_socket.library.WebSocketConnection;
import com.sergeymild.chat.services.web_socket.library.WebSocketException;
import com.sergeymild.chat.utils.AllyChatPreferences;
import com.sergeymild.chat.utils.DispatchUtil;
import com.sergeymild.chat.utils.Logger;

import java.net.URI;

import retrofit.RetrofitError;

/**
 * Created by sergeyMild on 16/11/15.
 */
public final class WebSocketChat extends WebSocket.SimpleWebSocketConnectionObserver implements WSChatApi {
    private WebSocketConnection webSocketConnection;

    private Handler handler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override public void run() {
            if (!isConnected()) {
                Logger.logi("try connect");
                connect();
            } else {
                Logger.logi("connected, remove callbacks");
                handler.removeCallbacks(runnable);
            }
        }
    };

    private WebSocketChat() {
        Logger.logi("WebSocketChat newInstance");
    }

    private static class InstanceHolder {
        private static final WebSocketChat INSTANCE = new WebSocketChat();
    }

    public static WebSocketChat getInstance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public final void connect() {
        Logger.logi("websockets connect");
        if (webSocketConnection != null && webSocketConnection.isConnected()) {
            Logger.logi("websockets already connected");
            return;
        }
        webSocketConnection = new WebSocketConnection();
        try {
            URI uri = URI.create("wss://" + AllyChat.getInstance().getHost().replace("https://", "") + "/api/2/relay?token=" + AllyChat.getInstance().tokenResponse().getToken());
            Logger.logi("connect to websocketUr: ", uri.toString());
            webSocketConnection.connect(uri, this);

        } catch (WebSocketException e) {
            ChatUtils.notifyOnError(new ErrorState(e.getMessage(), -1));
        }
    }

    @Override
    public final boolean isConnected() {
        return webSocketConnection!= null && webSocketConnection.isConnected();
    }

    @Override
    public final void disconnect() {
        if (webSocketConnection == null) return;
        webSocketConnection.disconnect();
    }


    @Override
    public final void onTextMessage(@NonNull final String payload) {
        Message newMessage = new ParseFromString().parse(payload);
        Logger.logi("onTextMessage ", newMessage);
        ChatUtils.onMessage(newMessage);
        AllyChatPreferences.removeFromList(newMessage);
        MessageController.incrementCounter();
    }


    @Override
    public final void onOpen() {
        Logger.logi("websockets onOpen");
        webSocketConnection.schedulePing();
    }

    @Override
    public final void onError(@NonNull final String errorMessage, @NonNull final String payload) {
        Logger.logi("websockets onError");
        Message message = new ParseFromString().parse(payload);
        message.setStatus(MessageStatus.STATUS_FAILED);
        AllyChatPreferences.updateMessage(message);
        ChatUtils.notifyMessageStatusChanged(message);
        ChatUtils.notifyOnError(new ErrorState(Errors.FAILED_TO_SENDING_MESSAGE.message, Errors.FAILED_TO_SENDING_MESSAGE.status));
        MessageController.decrementCounter();
    }

    @Override @WorkerThread
    public final void sendMessage(@NonNull final Message message) {
        Logger.logi("sendMessage");
        webSocketConnection.sendTextMessage(new PrepareForSend().build(message));
        message.setStatus(MessageStatus.STATUS_SEND);
        ChatUtils.notifyMessageStatusChanged(message);
    }

    @Override
    public final void onClose(final WebSocketCloseNotification code, final String reason) {
        super.onClose(code, reason);
        Logger.logi(code.name(), reason);
        if (WebSocketCloseNotification.SERVER_ERROR.equals(code) && reason.contains("401")) {
            DispatchUtil.getInstance().backgroundQueue.postRunnable(new Runnable() {
                @Override
                public void run() {
                    Logger.logi("onClose Server Error");
                    SdkSettings sdkSettings = SdkSettings.getInstance();
                    try {
                        TokenResponse token = ChatUtils.getToken(sdkSettings.getAlias(), sdkSettings.getAppId());
                        Logger.logi("logger", "token received");
                        AllyChat.getInstance().setToken(token);
                        handler.postDelayed(runnable, 3000);
                    } catch (RetrofitError error1) {
                        Logger.logi("logger", "token not received");
                    }
                }
            });
        }
    }
}

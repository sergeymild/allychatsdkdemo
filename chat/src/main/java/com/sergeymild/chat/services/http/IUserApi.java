package com.sergeymild.chat.services.http;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;
import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.models.ChatParameters;
import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.models.User;

/**
 * Created by sergeyMild on 18/11/15.
 */
public interface IUserApi {
    void register(@NonNull String alias, @NonNull ChatParameters chatParameters, ChatCallback<User> chatCallback);
    @WorkerThread
    User registerSync(@NonNull String alias, @NonNull ChatParameters chatParameters);

    @WorkerThread
    TokenResponse getToken(@NonNull String alias, @NonNull String appId);
}

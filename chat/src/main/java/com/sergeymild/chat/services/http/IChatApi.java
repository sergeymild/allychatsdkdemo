package com.sergeymild.chat.services.http;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import java.util.List;

import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.MessageFile;
import com.sergeymild.chat.models.Room;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by sergeyMild on 17/11/15.
 */
public interface IChatApi {
    @WorkerThread List<Room> getRooms();
    @WorkerThread Room getSupportRoom();

    @WorkerThread List<Message> getMessages(@NonNull String roomId, boolean stackFromEnd, int limit);
    @WorkerThread List<Message> getMoreMessages(@NonNull String roomId, boolean stackFromEnd, int limit, int offset);
    @WorkerThread List<Message> getMoreByLastMessageIdMessages(@NonNull String roomId, boolean stackFromEnd, int limit, @NonNull String lastMessageId);


    @WorkerThread Response getUnreadCount(@NonNull String roomId);

    @WorkerThread MessageFile uploadFile(@NonNull TypedFile typedFile);
    @WorkerThread void readMessage(@NonNull String messageId);
}

package com.sergeymild.chat.services.http;

import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.text.TextUtils;

import com.sergeymild.chat.AllyChat;
import com.sergeymild.chat.utils.CountingTypedFile;
import com.sergeymild.chat.services.web_socket.api.WebSocketChat;
import com.sergeymild.chat.callbacks.ChatCallback;
import com.sergeymild.chat.callbacks.NetworkStateListener;
import com.sergeymild.chat.controllers.MessageController;
import com.sergeymild.chat.controllers.RoomController;
import com.sergeymild.chat.models.ChatParameters;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.models.Message;
import com.sergeymild.chat.models.MessageStatus;
import com.sergeymild.chat.models.Room;
import com.sergeymild.chat.models.TokenResponse;
import com.sergeymild.chat.models.User;
import com.sergeymild.chat.utils.AllyChatPreferences;
import com.sergeymild.chat.utils.Checks;
import com.sergeymild.chat.utils.DispatchUtil;
import com.sergeymild.chat.utils.Logger;

import java.util.List;

import static com.sergeymild.chat.utils.Checks.checkOnline;

/**
 * Created by sergeyMild on 18/11/15.
 */
public final class ChatUtils {

    private ChatUtils() {
        throw new RuntimeException("No instances");
    }

    /*
    *
    *   Support room
    *
    * */

    public static void getSupportRoom(@NonNull final ChatCallback<Room> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        RoomController.getSupportRoom(callback);
    }

    public static void getSupportRoomUnreadCount(@NonNull final ChatCallback<Integer> callback) {
        if (!Checks.checkAppInitialized(callback)) return;
        if (!checkOnline()) return;
        RoomController.getUnreadCount("support", callback);
    }


    /*
    *
    * Rooms
    *
    * */

    public static void getRooms(@NonNull final ChatCallback<List<Room>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        RoomController.getRooms(callback);
    }

    public static void getUnreadCount(@NonNull final String roomId, @NonNull final ChatCallback<Integer> callback) {
        if (!Checks.checkAppInitialized(callback)) return;
        if (!checkOnline()) return;
        RoomController.getUnreadCount(roomId, callback);
    }



    /*
    *
    *   GET Support Messages
    *
    * */

    public static void getSupportMessages(@NonNull final ChatCallback<List<Message>> callback) {
        getSupportMessages(true, 20, callback);
    }

    public static void getSupportMessages(final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        getSupportMessages(true, limit, callback);
    }

    public static void getSupportMessages(final boolean stackFromEnd, @NonNull final ChatCallback<List<Message>> callback) {
        getSupportMessages(stackFromEnd, 20, callback);
    }

    public static void getSupportMessages(final boolean stackFromEnd, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        MessageController.getMessages("support", stackFromEnd, limit, callback);
    }


    /*
    *
    *   GET Support More Messages
    *
    * */

    public static void getMoreSupportMessages(@NonNull final ChatCallback<List<Message>> callback) {
        getMoreSupportMessages(true, 20, callback);
    }

    public static void getMoreSupportMessages(final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        getMoreSupportMessages(true, limit, callback);
    }

    public static void getMoreSupportMessages(final boolean stackFromEnd, @NonNull final ChatCallback<List<Message>> callback) {
        getMoreSupportMessages(stackFromEnd, 20, callback);
    }

    public static void getMoreSupportMessages(final boolean stackFromEnd, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        MessageController.getMoreMessages("support", stackFromEnd, limit, callback);
    }

    public static void getMoreByLastMessageIdMessages(final int limit, @NonNull final String lastMessageId, @NonNull final ChatCallback<List<Message>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        MessageController.getMoreByLastMessageIdMessages("support", limit, lastMessageId, callback);
    }





    /*
    *
    *   GET Messages
    *
    * */

    public static void getMessages(@NonNull final String roomId, @NonNull final ChatCallback<List<Message>> callback) {
        getMessages(roomId, true, 20, callback);
    }

    public static void getMessages(@NonNull final String roomId, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        getMessages(roomId, true, limit, callback);
    }

    public static void getMessages(@NonNull final String roomId, final boolean stackFromEnd, @NonNull final ChatCallback<List<Message>> callback) {
        getMessages(roomId, stackFromEnd, 20, callback);
    }

    public static void getMessages(@NonNull final String roomId, final boolean stackFromEnd, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        MessageController.getMessages(roomId, stackFromEnd, limit, callback);
    }


    /*
    *
    *   GET More Messages
    *
    * */

    public static void getMoreMessages(@NonNull final String roomId, @NonNull final ChatCallback<List<Message>> callback) {
        getMoreMessages(roomId, true, 20, callback);
    }

    public static void getMoreMessages(@NonNull final String roomId, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        getMoreMessages(roomId, true, limit, callback);
    }

    public static void getMoreMessages(@NonNull final String roomId, final boolean stackFromEnd, @NonNull final ChatCallback<List<Message>> callback) {
        getMoreMessages(roomId, stackFromEnd, 20, callback);
    }

    public static void getMoreMessages(@NonNull final String roomId, final boolean stackFromEnd, final int limit, @NonNull final ChatCallback<List<Message>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        MessageController.getMoreMessages(roomId, stackFromEnd, limit, callback);
    }

    public static void getMoreByLastMessageIdMessages(@NonNull final String roomId, final int limit, @NonNull final String lastMessageId, @NonNull final ChatCallback<List<Message>> callback) {
        if (!Checks.checkAppInitialized(callback) || !checkOnline()) return;
        MessageController.getMoreByLastMessageIdMessages(roomId, limit, lastMessageId, callback);
    }

    public static void readMessage(@NonNull final Message message) {
        if (!Checks.checkAppInitialized(null) || !checkOnline()) return;
        MessageController.readMessage(message.getId());
    }


    public static void sendMessage(@NonNull final Message message) {
        //if (!Checks.checkAppInitialized(null) || !checkOnline()) return;
        message.setStatus(MessageStatus.STATUS_SENDING);
        MessageController.sendMessage(message);
    }

    public static void resendMessage(@NonNull final Message message) {
        if (!Checks.checkAppInitialized(null) || !checkOnline()) return;
        message.setCreatedAtMillis(System.currentTimeMillis());
        message.setStatus(MessageStatus.STATUS_RESENDING);
        MessageController.sendMessage(message);
    }

    public static void buildAndSend(@NonNull final String roomId, @NonNull final String messageText, @Nullable final String filePath) {
        //if (!Checks.checkAppInitialized(null) || !checkOnline()) return;
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                sendMessage(buildMessage(roomId, messageText, filePath));
            }
        });
    }

    public static void buildAndSend(@NonNull final String roomId, @NonNull final String messageText, @Nullable final String filePath, final CountingTypedFile.ProgressListener progressListener) {
        //if (!Checks.checkAppInitialized(null) || !checkOnline()) return;
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                Message message = buildMessage(roomId, messageText, filePath);
                message.setStatus(MessageStatus.STATUS_SENDING);
                MessageController.sendMessage(message, progressListener);
            }
        });
    }

    public static Message buildMessage(@NonNull final String messageText) {
        return buildMessage("support", messageText, null);
    }

    public static Message buildMessage(@NonNull final String messageText, @Nullable final String filePath) {
        return buildMessage("support", messageText, filePath);
    }

    public static Message buildMessage(@NonNull final String roomId, @NonNull final String messageText, @Nullable final String filePath) {
        return new Message.Builder()
                .setMessage(messageText)
                .setCreatedAt(System.currentTimeMillis())
                .setFilePath(filePath)
                .setRoom(roomId)
                .build();
    }

    /*
    *
    * WebSocket
    *
    * */


    @WorkerThread
    public static void onMessage(@NonNull final Message message) {
        if (!TextUtils.isEmpty(message.getEvent())) notifyMessageEventChanged(message);
        ServiceFactory.getChatEventEmitter().onMessage(message);
    }

    /*
    *
    *  PUSH
    *
    * */

    @WorkerThread
    public static void registerGcm(@NonNull final String token) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                AllyChatPreferences.setGcmToken(token);
                ServiceFactory.getGcmApi().registerGcm(AllyChat.getInstance().getAndroidId(), token);
            }
        });
    }

    @WorkerThread
    public static void unRegisterGcm(@NonNull final String token) {
        DispatchUtil.getInstance().networkQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                ServiceFactory.getGcmApi().unRegisterGcm(AllyChat.getInstance().getAndroidId());
            }
        });
    }


    /*
    *
    * Users
    *
    * */


    @CheckResult
    public static String userId() {
        return AllyChat.getInstance().getUser().getId();
    }

    @CheckResult
    public static User getUser() {
        return AllyChatPreferences.getUser();
    }

    public static void register(@NonNull final String alias, @NonNull final ChatParameters chatParameters, @NonNull final ChatCallback<User> chatCallback) {
        ServiceFactory.getUserApi().register(alias, chatParameters, chatCallback);
    }

    @WorkerThread
    @CheckResult
    public static User registerSync(@NonNull final String alias, @NonNull final ChatParameters chatParameters) {
        return ServiceFactory.getUserApi().registerSync(alias, chatParameters);
    }

    @WorkerThread
    @CheckResult
    public static TokenResponse getToken(@NonNull final String alias, @NonNull final String appId) {
        return ServiceFactory.getUserApi().getToken(alias, appId);
    }



    /*
    *
    *   Notifications
    *
    * */


    public static void notifyMessageStatusChanged(@NonNull final Message message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ServiceFactory.getChatEventEmitter().onMessageStatusChanged(message);
            }
        });
    }

    public static void notifyMessageEventChanged(@NonNull final Message message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ServiceFactory.getChatEventEmitter().onMessageEventStatusChanged(message);
            }
        });
    }

    public static void subscribeOnNetworkStateChange(@NonNull final NetworkStateListener networkState) {
        ServiceFactory.getChatEventEmitter().subscribeOnNetworkStateChange(networkState);
    }

    public static void unSubscribeOnNetworkStateChange(@NonNull final NetworkStateListener networkState) {
        ServiceFactory.getChatEventEmitter().unSubscribeOnNetworkStateChange(networkState);
    }

    public static void notifyNetworkStateChanged(final boolean isConnected) {
        ServiceFactory.getChatEventEmitter().postNetworkStateChanged(isConnected);
    }

    public static void notifyOnError(@NonNull final ErrorState errorState) {
        ServiceFactory.getChatEventEmitter().postOnError(errorState);
    }




    public static void registerListeners(@NonNull final Object object) {
        DispatchUtil.getInstance().backgroundQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                Logger.logi("registerListeners");
                ServiceFactory.getChatEventEmitter().register(object);
                if (AllyChat.getInstance().isInitialized())
                    MessageController.setSendingMessages();
            }
        });
        if (AllyChat.getInstance().isInitialized()) {
            ServiceFactory.getWsChatApi().connect();
        }
    }

    public static void clearSession(final boolean clearOffset) {
        MessageController.clearCurrentSession(clearOffset);
    }

    public static void unRegisterListeners(@NonNull final Object object) {
        DispatchUtil.getInstance().backgroundQueue.postRunnable(new Runnable() {
            @Override
            public void run() {
                Logger.logi("unRegisterListeners");
                ServiceFactory.getChatEventEmitter().unregister(object);
            }
        });
        ServiceFactory.getWsChatApi().disconnect();
    }

    public static void logOut() {
        ServiceFactory.getChatEventEmitter().unregisterAll();
        ServiceFactory.getWsChatApi().disconnect();
        MessageController.clearCurrentSession(true);
        AllyChat.getInstance().clear();
    }



    @WorkerThread
    private static void runOnUiThread(@NonNull final Runnable runnable) {
        if (DispatchUtil.getInstance().isMainThread()) {
            runnable.run();
        } else {
            DispatchUtil.getInstance().runOnUiThread(runnable);
        }
    }



    public static void saveLastMessageId(@NonNull final String roomId, @NonNull final String lastMessageId) {
        AllyChatPreferences.saveLastMessageId(roomId, lastMessageId);
    }

    public static String getSavedLastMessageId(@NonNull final String roomId) {
        return AllyChatPreferences.getLastMessageId(roomId);
    }

    public static void removeSavedLastMessageId(@NonNull final String roomId) {
        AllyChatPreferences.removeLastMessageId(roomId);
    }
}

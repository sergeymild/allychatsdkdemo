package com.sergeymild.chat.callbacks;


import com.sergeymild.chat.models.ErrorState;

/**
 * Created by sergeyMild on 12/11/15.
 */
public interface OnFailureInitialize {
    void onFailInitialize(ErrorState errorState);
}

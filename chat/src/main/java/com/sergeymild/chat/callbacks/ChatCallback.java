package com.sergeymild.chat.callbacks;

import android.support.annotation.CallSuper;
import android.support.annotation.CheckResult;
import android.support.annotation.NonNull;

import com.sergeymild.chat.convertors.ErrorConvertor;
import com.sergeymild.chat.models.ErrorState;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by sergeyMild on 17/11/15.
 */
public abstract class ChatCallback<T> implements retrofit.Callback<T> {

    @Override
    public void success(T t, @NonNull final Response response) {
        this.success(t);
    }

    @Override
    public void failure(@NonNull final RetrofitError error) {
        this.failure(processError(error));
    }

    @CallSuper @CheckResult
    private ErrorState processError(@NonNull final RetrofitError error) {
        return new ErrorConvertor(error).parse();
    }

    public abstract void success(T result);
    public abstract void failure(final ErrorState errorState);
}

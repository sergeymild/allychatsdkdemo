package com.sergeymild.chat.callbacks;

import android.support.annotation.NonNull;
import com.sergeymild.chat.AllyChat;


/**
 * Created by sergeyMild on 12/11/15.
 */
public interface OnSuccessInitialize {
    void onSuccess(@NonNull AllyChat chat);
}

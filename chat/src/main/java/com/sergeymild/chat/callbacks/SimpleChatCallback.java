package com.sergeymild.chat.callbacks;

import android.support.annotation.CallSuper;
import com.sergeymild.chat.models.ErrorState;
import com.sergeymild.chat.utils.Logger;

/**
 * Created by sergeyMild on 17/11/15.
 */
public class SimpleChatCallback<T> extends ChatCallback<T> {
    @Override @CallSuper
    public void success(T result) {}

    @Override @CallSuper
    public void failure(ErrorState errorState) {
        Logger.logi(errorState.getMessage(), errorState.getCode());
    }
}

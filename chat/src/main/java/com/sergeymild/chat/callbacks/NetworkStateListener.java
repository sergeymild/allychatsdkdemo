package com.sergeymild.chat.callbacks;

/**
 * Created by sergeyMild on 22/11/15.
 */
public interface NetworkStateListener {
    void onNetworkStateChanged(boolean isConnected);
}
